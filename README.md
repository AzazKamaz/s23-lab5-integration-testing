# Lab5 -- Integration testing


## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it can be used both for whitebox and blackbox testing, depending on your goals and possibilities.   ***Let's roll!***🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were doing integration tests, usually it is a task for the automated testing team, to check integration of one module with another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough, because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.


## Lab
You can find your links in table for spring 2023 [here](https://docs.google.com/spreadsheets/d/1YYFY0XtzODmYxZJDKaEfSPipa6OYL-ps9a6X0GTO9SU/edit#gid=147093893)

Ok, finally, we won't need to write any code today :). Hope you're happy)
1. Create your fork of the `
Lab5 - Integration testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/s23-lab5-integration-testing)
2. Try to use InnoDrive application:
- At first we need to check our default parameters, for it we will need some REST client preferrably(Insomnia, Postman or you may use your browser), install it. 
- Then let's read through the description of our system:
    The InnoDrive is a hypothetical car sharing service that provides transportations means in Kazan region including Innopolis. The service proposes two types of cars `budget` and `luxury`. The service offers flexible two tariff plans. With the `minute` plan, the service charges its client by `time per minute of use`. The Service also proposes a `fixed_price` plan whereas the price is fixed at the reservation time when the route is chosen. If the driver deviates from the planned route for more than `n%`, the tariff plan automatically switches to the `minute` plan. The deviation is calculated as difference in the distance or duration from the originally planned route. The `fixed_price` plan is available for `budget` cars only. The Innopolis city sponsors the mobility and offers `m%` discount to Innopolis residents.
    Imagine you are the quality engineer of Innodrive. How are you going to know about application quality???
- InnoDrive application is just a web application that allows you to compute the price for a certain car ride
- To get particularly your defaults, let's send this request:
    `https://script.google.com/macros/s/_your_key_/exec?service=getSpec&email=_your_email_` 
  This will return our default spec(your result might be different):
        Here is InnoCar Specs:

        Budet car price per minute = 17

        Luxury car price per minute = 33

        Fixed price per km = 11

        Allowed deviations in % = 10

        Inno discount in % = 10

- To get the price for certain car ride, let's send next request:
`https://script.google.com/macros/s/_your_key_/exec?service=calculatePrice&email=_your_email_&type=_type_&plan=_plan_&distance=_distance_&planned_distance=_planned_distance_&time=_time_&planned_time=_planned_time_&inno_discount=_discount_` with some random parameters. Answer should be like `{"price": 100}` or `Invalid Request`
- And next we can create our BVA table, this is a lab, so I will create it just for 2 parameters: 
`distance` and `type`.
 `distance` is an integer value, so we can build 2 equivalence classes:
 + `distance` <= 0
 + `distance` > 0
 while `type` have 3 equivalence classes:
 + `budget` 
 + `luxury`
 + or some `nonsense`.

- Let's use BVA to make integration tests,we need to make few different testcases with `distance`, depending on its value: 
 + `distance` <= 0 : -10 0
 + `distance` > 0 : 1 100000
This way we will test both our borders and our normal values, we can do the same thing for `type`:
 + `type` = "budget" 
 + `type` = "luxury" 
 + `type` = "Guten morgen sonnenschein" 

- Now, let's check responses for different values of our parameters, with all other parameters already setted up, so we will have:
  + `plan` = `minute` 
  + `planned_distance` = `100` 
  + `time` = `110` 
  + `planned_time` = `100` 
  + `inno_discount` = `yes`
Let's build test cases out of this data:

| Test case  | distance |   type    | Expected result      |
|------------|----------|-----------|----------------------|
|     1      |    -10   |     *     |   Invalid Request    |
|     2      |    0     |     *     |   Invalid Request    |
|     3      |    *     | "nonsense"|   Invalid Request    |
|     4      |    1     |"budget"   |   1683               |
|     5      |    1     |"luxury"   |   3267               |
|     6      | 1000000  |"budget"   |   1683               |
|     7      | 1000000  |"luxury"   |   3267               |

etc...(there are a lot of combinations), to pick only needed one it is a good practice to use specialized tools.
Let's use Decision table to cut out our tests:
| Conditions(inputs)  |             Values           |    R1    |   R2    |   R3  |    R4   |
|---------------------|------------------------------|----------|---------|-------|---------|
|     Type            | "budget","luxury", nonsense  | nonsense |  budget | luxury|    *    |
|     Distance        | >0, <=0                      |     *    |  >0     |  >0   |    <=0  |
|---------------------|------------------------------|----------|---------|-------|---------|
|  Invalid Request    |                              |    X     |         |       |    X    |
|     200             |                              |          |    X    |   X   |         |

Now let's use this request to test our values




## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch. 

Also you have to provide some kind of estimation of coverage you have done

Note: You can use any automated solution, but in this case I can ask whether you are sure that this solution does not contain the same bugs

Note 2: 10% of lab grade would be soundness of bugs you have found(in realtion to total amount of bugs injected in your version)


## Specs
```
Here is InnoCar Specs:
Budet car price per minute = 20
Luxury car price per minute = 40
Fixed price per km = 11
Allowed deviations in % = 7.000000000000001
Inno discount in % = 9
```

## BVA
```python
parameters_tested = {
    'type': ['budget', 'luxury']
    'plan': ['minute', 'fixed_price']
    'distance': [0, 10, 20]
    'planned_distance': [0, 10]
    'time': [0, 10, 20]
    'planned_time': [0, 10]
    'inno_discount': ['no', 'yes']
}
```
## Tests

| $id | type | plan | distance | planned_distance | time | planned_time | inno_discount | $result | $expected | $ok |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 0 | budget | minute | 0 | 0 | 0 | 0 | no | 0 | 0 | True |
| 1 | luxury | minute | 0 | 0 | 0 | 0 | no | 0 | 0 | True |
| 2 | budget | fixed_price | 0 | 0 | 0 | 0 | no | 0 | 0 | True |
| 3 | luxury | fixed_price | 0 | 0 | 0 | 0 | no | Invalid Request | Invalid Request | True |
| 4 | budget | minute | 10 | 0 | 0 | 0 | no | 0 | 0 | True |
| 5 | luxury | minute | 10 | 0 | 0 | 0 | no | 0 | 0 | True |
| 6 | budget | fixed_price | 10 | 0 | 0 | 0 | no | 0 | 0 | True |
| 7 | luxury | fixed_price | 10 | 0 | 0 | 0 | no | Invalid Request | Invalid Request | True |
| 8 | budget | minute | 20 | 0 | 0 | 0 | no | 0 | 0 | True |
| 9 | luxury | minute | 20 | 0 | 0 | 0 | no | 0 | 0 | True |
| 10 | budget | fixed_price | 20 | 0 | 0 | 0 | no | 0 | 0 | True |
| 11 | luxury | fixed_price | 20 | 0 | 0 | 0 | no | Invalid Request | Invalid Request | True |
| 12 | budget | minute | 0 | 10 | 0 | 0 | no | 0 | 0 | True |
| 13 | luxury | minute | 0 | 10 | 0 | 0 | no | 0 | 0 | True |
| 14 | budget | fixed_price | 0 | 10 | 0 | 0 | no | 0 | 0 | True |
| 15 | luxury | fixed_price | 0 | 10 | 0 | 0 | no | Invalid Request | Invalid Request | True |
| 16 | budget | minute | 10 | 10 | 0 | 0 | no | 0 | 0 | True |
| 17 | luxury | minute | 10 | 10 | 0 | 0 | no | 0 | 0 | True |
| 18 | budget | fixed_price | 10 | 10 | 0 | 0 | no | 0 | 110 | False |
| 19 | luxury | fixed_price | 10 | 10 | 0 | 0 | no | Invalid Request | Invalid Request | True |
| 20 | budget | minute | 20 | 10 | 0 | 0 | no | 0 | 0 | True |
| 21 | luxury | minute | 20 | 10 | 0 | 0 | no | 0 | 0 | True |
| 22 | budget | fixed_price | 20 | 10 | 0 | 0 | no | 0 | 0 | True |
| 23 | luxury | fixed_price | 20 | 10 | 0 | 0 | no | Invalid Request | Invalid Request | True |
| 24 | budget | minute | 0 | 0 | 10 | 0 | no | 260 | 200 | False |
| 25 | luxury | minute | 0 | 0 | 10 | 0 | no | 400 | 400 | True |
| 26 | budget | fixed_price | 0 | 0 | 10 | 0 | no | 166.66666666666666 | 200 | False |
| 27 | luxury | fixed_price | 0 | 0 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 28 | budget | minute | 10 | 0 | 10 | 0 | no | 260 | 200 | False |
| 29 | luxury | minute | 10 | 0 | 10 | 0 | no | 400 | 400 | True |
| 30 | budget | fixed_price | 10 | 0 | 10 | 0 | no | 166.66666666666666 | 200 | False |
| 31 | luxury | fixed_price | 10 | 0 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 32 | budget | minute | 20 | 0 | 10 | 0 | no | 260 | 200 | False |
| 33 | luxury | minute | 20 | 0 | 10 | 0 | no | 400 | 400 | True |
| 34 | budget | fixed_price | 20 | 0 | 10 | 0 | no | 166.66666666666666 | 200 | False |
| 35 | luxury | fixed_price | 20 | 0 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 36 | budget | minute | 0 | 10 | 10 | 0 | no | 260 | 200 | False |
| 37 | luxury | minute | 0 | 10 | 10 | 0 | no | 400 | 400 | True |
| 38 | budget | fixed_price | 0 | 10 | 10 | 0 | no | 166.66666666666666 | 200 | False |
| 39 | luxury | fixed_price | 0 | 10 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 40 | budget | minute | 10 | 10 | 10 | 0 | no | 260 | 200 | False |
| 41 | luxury | minute | 10 | 10 | 10 | 0 | no | 400 | 400 | True |
| 42 | budget | fixed_price | 10 | 10 | 10 | 0 | no | 166.66666666666666 | 200 | False |
| 43 | luxury | fixed_price | 10 | 10 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 44 | budget | minute | 20 | 10 | 10 | 0 | no | 260 | 200 | False |
| 45 | luxury | minute | 20 | 10 | 10 | 0 | no | 400 | 400 | True |
| 46 | budget | fixed_price | 20 | 10 | 10 | 0 | no | 166.66666666666666 | 200 | False |
| 47 | luxury | fixed_price | 20 | 10 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 48 | budget | minute | 0 | 0 | 20 | 0 | no | 520 | 400 | False |
| 49 | luxury | minute | 0 | 0 | 20 | 0 | no | 800 | 800 | True |
| 50 | budget | fixed_price | 0 | 0 | 20 | 0 | no | 333.3333333333333 | 400 | False |
| 51 | luxury | fixed_price | 0 | 0 | 20 | 0 | no | Invalid Request | Invalid Request | True |
| 52 | budget | minute | 10 | 0 | 20 | 0 | no | 520 | 400 | False |
| 53 | luxury | minute | 10 | 0 | 20 | 0 | no | 800 | 800 | True |
| 54 | budget | fixed_price | 10 | 0 | 20 | 0 | no | 333.3333333333333 | 400 | False |
| 55 | luxury | fixed_price | 10 | 0 | 20 | 0 | no | Invalid Request | Invalid Request | True |
| 56 | budget | minute | 20 | 0 | 20 | 0 | no | 520 | 400 | False |
| 57 | luxury | minute | 20 | 0 | 20 | 0 | no | 800 | 800 | True |
| 58 | budget | fixed_price | 20 | 0 | 20 | 0 | no | 333.3333333333333 | 400 | False |
| 59 | luxury | fixed_price | 20 | 0 | 20 | 0 | no | Invalid Request | Invalid Request | True |
| 60 | budget | minute | 0 | 10 | 20 | 0 | no | 520 | 400 | False |
| 61 | luxury | minute | 0 | 10 | 20 | 0 | no | 800 | 800 | True |
| 62 | budget | fixed_price | 0 | 10 | 20 | 0 | no | 333.3333333333333 | 400 | False |
| 63 | luxury | fixed_price | 0 | 10 | 20 | 0 | no | Invalid Request | Invalid Request | True |
| 64 | budget | minute | 10 | 10 | 20 | 0 | no | 520 | 400 | False |
| 65 | luxury | minute | 10 | 10 | 20 | 0 | no | 800 | 800 | True |
| 66 | budget | fixed_price | 10 | 10 | 20 | 0 | no | 333.3333333333333 | 400 | False |
| 67 | luxury | fixed_price | 10 | 10 | 20 | 0 | no | Invalid Request | Invalid Request | True |
| 68 | budget | minute | 20 | 10 | 20 | 0 | no | 520 | 400 | False |
| 69 | luxury | minute | 20 | 10 | 20 | 0 | no | 800 | 800 | True |
| 70 | budget | fixed_price | 20 | 10 | 20 | 0 | no | 333.3333333333333 | 400 | False |
| 71 | luxury | fixed_price | 20 | 10 | 20 | 0 | no | Invalid Request | Invalid Request | True |
| 72 | budget | minute | 0 | 0 | 0 | 10 | no | 0 | 0 | True |
| 73 | luxury | minute | 0 | 0 | 0 | 10 | no | 0 | 0 | True |
| 74 | budget | fixed_price | 0 | 0 | 0 | 10 | no | 0 | 0 | True |
| 75 | luxury | fixed_price | 0 | 0 | 0 | 10 | no | Invalid Request | Invalid Request | True |
| 76 | budget | minute | 10 | 0 | 0 | 10 | no | 0 | 0 | True |
| 77 | luxury | minute | 10 | 0 | 0 | 10 | no | 0 | 0 | True |
| 78 | budget | fixed_price | 10 | 0 | 0 | 10 | no | 0 | 0 | True |
| 79 | luxury | fixed_price | 10 | 0 | 0 | 10 | no | Invalid Request | Invalid Request | True |
| 80 | budget | minute | 20 | 0 | 0 | 10 | no | 0 | 0 | True |
| 81 | luxury | minute | 20 | 0 | 0 | 10 | no | 0 | 0 | True |
| 82 | budget | fixed_price | 20 | 0 | 0 | 10 | no | 0 | 0 | True |
| 83 | luxury | fixed_price | 20 | 0 | 0 | 10 | no | Invalid Request | Invalid Request | True |
| 84 | budget | minute | 0 | 10 | 0 | 10 | no | 0 | 0 | True |
| 85 | luxury | minute | 0 | 10 | 0 | 10 | no | 0 | 0 | True |
| 86 | budget | fixed_price | 0 | 10 | 0 | 10 | no | 0 | 0 | True |
| 87 | luxury | fixed_price | 0 | 10 | 0 | 10 | no | Invalid Request | Invalid Request | True |
| 88 | budget | minute | 10 | 10 | 0 | 10 | no | 0 | 0 | True |
| 89 | luxury | minute | 10 | 10 | 0 | 10 | no | 0 | 0 | True |
| 90 | budget | fixed_price | 10 | 10 | 0 | 10 | no | 0 | 0 | True |
| 91 | luxury | fixed_price | 10 | 10 | 0 | 10 | no | Invalid Request | Invalid Request | True |
| 92 | budget | minute | 20 | 10 | 0 | 10 | no | 0 | 0 | True |
| 93 | luxury | minute | 20 | 10 | 0 | 10 | no | 0 | 0 | True |
| 94 | budget | fixed_price | 20 | 10 | 0 | 10 | no | 0 | 0 | True |
| 95 | luxury | fixed_price | 20 | 10 | 0 | 10 | no | Invalid Request | Invalid Request | True |
| 96 | budget | minute | 0 | 0 | 10 | 10 | no | 260 | 200 | False |
| 97 | luxury | minute | 0 | 0 | 10 | 10 | no | 400 | 400 | True |
| 98 | budget | fixed_price | 0 | 0 | 10 | 10 | no | 166.66666666666666 | 0 | False |
| 99 | luxury | fixed_price | 0 | 0 | 10 | 10 | no | Invalid Request | Invalid Request | True |
| 100 | budget | minute | 10 | 0 | 10 | 10 | no | 260 | 200 | False |
| 101 | luxury | minute | 10 | 0 | 10 | 10 | no | 400 | 400 | True |
| 102 | budget | fixed_price | 10 | 0 | 10 | 10 | no | 166.66666666666666 | 200 | False |
| 103 | luxury | fixed_price | 10 | 0 | 10 | 10 | no | Invalid Request | Invalid Request | True |
| 104 | budget | minute | 20 | 0 | 10 | 10 | no | 260 | 200 | False |
| 105 | luxury | minute | 20 | 0 | 10 | 10 | no | 400 | 400 | True |
| 106 | budget | fixed_price | 20 | 0 | 10 | 10 | no | 166.66666666666666 | 200 | False |
| 107 | luxury | fixed_price | 20 | 0 | 10 | 10 | no | Invalid Request | Invalid Request | True |
| 108 | budget | minute | 0 | 10 | 10 | 10 | no | 260 | 200 | False |
| 109 | luxury | minute | 0 | 10 | 10 | 10 | no | 400 | 400 | True |
| 110 | budget | fixed_price | 0 | 10 | 10 | 10 | no | 166.66666666666666 | 200 | False |
| 111 | luxury | fixed_price | 0 | 10 | 10 | 10 | no | Invalid Request | Invalid Request | True |
| 112 | budget | minute | 10 | 10 | 10 | 10 | no | 260 | 200 | False |
| 113 | luxury | minute | 10 | 10 | 10 | 10 | no | 400 | 400 | True |
| 114 | budget | fixed_price | 10 | 10 | 10 | 10 | no | 125 | 110 | False |
| 115 | luxury | fixed_price | 10 | 10 | 10 | 10 | no | Invalid Request | Invalid Request | True |
| 116 | budget | minute | 20 | 10 | 10 | 10 | no | 260 | 200 | False |
| 117 | luxury | minute | 20 | 10 | 10 | 10 | no | 400 | 400 | True |
| 118 | budget | fixed_price | 20 | 10 | 10 | 10 | no | 166.66666666666666 | 200 | False |
| 119 | luxury | fixed_price | 20 | 10 | 10 | 10 | no | Invalid Request | Invalid Request | True |
| 120 | budget | minute | 0 | 0 | 20 | 10 | no | 520 | 400 | False |
| 121 | luxury | minute | 0 | 0 | 20 | 10 | no | 800 | 800 | True |
| 122 | budget | fixed_price | 0 | 0 | 20 | 10 | no | 333.3333333333333 | 400 | False |
| 123 | luxury | fixed_price | 0 | 0 | 20 | 10 | no | Invalid Request | Invalid Request | True |
| 124 | budget | minute | 10 | 0 | 20 | 10 | no | 520 | 400 | False |
| 125 | luxury | minute | 10 | 0 | 20 | 10 | no | 800 | 800 | True |
| 126 | budget | fixed_price | 10 | 0 | 20 | 10 | no | 333.3333333333333 | 400 | False |
| 127 | luxury | fixed_price | 10 | 0 | 20 | 10 | no | Invalid Request | Invalid Request | True |
| 128 | budget | minute | 20 | 0 | 20 | 10 | no | 520 | 400 | False |
| 129 | luxury | minute | 20 | 0 | 20 | 10 | no | 800 | 800 | True |
| 130 | budget | fixed_price | 20 | 0 | 20 | 10 | no | 333.3333333333333 | 400 | False |
| 131 | luxury | fixed_price | 20 | 0 | 20 | 10 | no | Invalid Request | Invalid Request | True |
| 132 | budget | minute | 0 | 10 | 20 | 10 | no | 520 | 400 | False |
| 133 | luxury | minute | 0 | 10 | 20 | 10 | no | 800 | 800 | True |
| 134 | budget | fixed_price | 0 | 10 | 20 | 10 | no | 333.3333333333333 | 400 | False |
| 135 | luxury | fixed_price | 0 | 10 | 20 | 10 | no | Invalid Request | Invalid Request | True |
| 136 | budget | minute | 10 | 10 | 20 | 10 | no | 520 | 400 | False |
| 137 | luxury | minute | 10 | 10 | 20 | 10 | no | 800 | 800 | True |
| 138 | budget | fixed_price | 10 | 10 | 20 | 10 | no | 333.3333333333333 | 400 | False |
| 139 | luxury | fixed_price | 10 | 10 | 20 | 10 | no | Invalid Request | Invalid Request | True |
| 140 | budget | minute | 20 | 10 | 20 | 10 | no | 520 | 400 | False |
| 141 | luxury | minute | 20 | 10 | 20 | 10 | no | 800 | 800 | True |
| 142 | budget | fixed_price | 20 | 10 | 20 | 10 | no | 333.3333333333333 | 400 | False |
| 143 | luxury | fixed_price | 20 | 10 | 20 | 10 | no | Invalid Request | Invalid Request | True |
| 144 | budget | minute | 0 | 0 | 0 | 0 | yes | 0 | 0.0 | True |
| 145 | luxury | minute | 0 | 0 | 0 | 0 | yes | 0 | 0.0 | True |
| 146 | budget | fixed_price | 0 | 0 | 0 | 0 | yes | 0 | 0.0 | True |
| 147 | luxury | fixed_price | 0 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request | True |
| 148 | budget | minute | 10 | 0 | 0 | 0 | yes | 0 | 0.0 | True |
| 149 | luxury | minute | 10 | 0 | 0 | 0 | yes | 0 | 0.0 | True |
| 150 | budget | fixed_price | 10 | 0 | 0 | 0 | yes | 0 | 0.0 | True |
| 151 | luxury | fixed_price | 10 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request | True |
| 152 | budget | minute | 20 | 0 | 0 | 0 | yes | 0 | 0.0 | True |
| 153 | luxury | minute | 20 | 0 | 0 | 0 | yes | 0 | 0.0 | True |
| 154 | budget | fixed_price | 20 | 0 | 0 | 0 | yes | 0 | 0.0 | True |
| 155 | luxury | fixed_price | 20 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request | True |
| 156 | budget | minute | 0 | 10 | 0 | 0 | yes | 0 | 0.0 | True |
| 157 | luxury | minute | 0 | 10 | 0 | 0 | yes | 0 | 0.0 | True |
| 158 | budget | fixed_price | 0 | 10 | 0 | 0 | yes | 0 | 0.0 | True |
| 159 | luxury | fixed_price | 0 | 10 | 0 | 0 | yes | Invalid Request | Invalid Request | True |
| 160 | budget | minute | 10 | 10 | 0 | 0 | yes | 0 | 0.0 | True |
| 161 | luxury | minute | 10 | 10 | 0 | 0 | yes | 0 | 0.0 | True |
| 162 | budget | fixed_price | 10 | 10 | 0 | 0 | yes | 0 | 100.10000000000001 | False |
| 163 | luxury | fixed_price | 10 | 10 | 0 | 0 | yes | Invalid Request | Invalid Request | True |
| 164 | budget | minute | 20 | 10 | 0 | 0 | yes | 0 | 0.0 | True |
| 165 | luxury | minute | 20 | 10 | 0 | 0 | yes | 0 | 0.0 | True |
| 166 | budget | fixed_price | 20 | 10 | 0 | 0 | yes | 0 | 0.0 | True |
| 167 | luxury | fixed_price | 20 | 10 | 0 | 0 | yes | Invalid Request | Invalid Request | True |
| 168 | budget | minute | 0 | 0 | 10 | 0 | yes | 260 | 182.0 | False |
| 169 | luxury | minute | 0 | 0 | 10 | 0 | yes | 400 | 364.0 | False |
| 170 | budget | fixed_price | 0 | 0 | 10 | 0 | yes | 166.66666666666666 | 182.0 | False |
| 171 | luxury | fixed_price | 0 | 0 | 10 | 0 | yes | Invalid Request | Invalid Request | True |
| 172 | budget | minute | 10 | 0 | 10 | 0 | yes | 260 | 182.0 | False |
| 173 | luxury | minute | 10 | 0 | 10 | 0 | yes | 400 | 364.0 | False |
| 174 | budget | fixed_price | 10 | 0 | 10 | 0 | yes | 166.66666666666666 | 182.0 | False |
| 175 | luxury | fixed_price | 10 | 0 | 10 | 0 | yes | Invalid Request | Invalid Request | True |
| 176 | budget | minute | 20 | 0 | 10 | 0 | yes | 260 | 182.0 | False |
| 177 | luxury | minute | 20 | 0 | 10 | 0 | yes | 400 | 364.0 | False |
| 178 | budget | fixed_price | 20 | 0 | 10 | 0 | yes | 166.66666666666666 | 182.0 | False |
| 179 | luxury | fixed_price | 20 | 0 | 10 | 0 | yes | Invalid Request | Invalid Request | True |
| 180 | budget | minute | 0 | 10 | 10 | 0 | yes | 260 | 182.0 | False |
| 181 | luxury | minute | 0 | 10 | 10 | 0 | yes | 400 | 364.0 | False |
| 182 | budget | fixed_price | 0 | 10 | 10 | 0 | yes | 166.66666666666666 | 182.0 | False |
| 183 | luxury | fixed_price | 0 | 10 | 10 | 0 | yes | Invalid Request | Invalid Request | True |
| 184 | budget | minute | 10 | 10 | 10 | 0 | yes | 260 | 182.0 | False |
| 185 | luxury | minute | 10 | 10 | 10 | 0 | yes | 400 | 364.0 | False |
| 186 | budget | fixed_price | 10 | 10 | 10 | 0 | yes | 166.66666666666666 | 182.0 | False |
| 187 | luxury | fixed_price | 10 | 10 | 10 | 0 | yes | Invalid Request | Invalid Request | True |
| 188 | budget | minute | 20 | 10 | 10 | 0 | yes | 260 | 182.0 | False |
| 189 | luxury | minute | 20 | 10 | 10 | 0 | yes | 400 | 364.0 | False |
| 190 | budget | fixed_price | 20 | 10 | 10 | 0 | yes | 166.66666666666666 | 182.0 | False |
| 191 | luxury | fixed_price | 20 | 10 | 10 | 0 | yes | Invalid Request | Invalid Request | True |
| 192 | budget | minute | 0 | 0 | 20 | 0 | yes | 520 | 364.0 | False |
| 193 | luxury | minute | 0 | 0 | 20 | 0 | yes | 800 | 728.0 | False |
| 194 | budget | fixed_price | 0 | 0 | 20 | 0 | yes | 333.3333333333333 | 364.0 | False |
| 195 | luxury | fixed_price | 0 | 0 | 20 | 0 | yes | Invalid Request | Invalid Request | True |
| 196 | budget | minute | 10 | 0 | 20 | 0 | yes | 520 | 364.0 | False |
| 197 | luxury | minute | 10 | 0 | 20 | 0 | yes | 800 | 728.0 | False |
| 198 | budget | fixed_price | 10 | 0 | 20 | 0 | yes | 333.3333333333333 | 364.0 | False |
| 199 | luxury | fixed_price | 10 | 0 | 20 | 0 | yes | Invalid Request | Invalid Request | True |
| 200 | budget | minute | 20 | 0 | 20 | 0 | yes | 520 | 364.0 | False |
| 201 | luxury | minute | 20 | 0 | 20 | 0 | yes | 800 | 728.0 | False |
| 202 | budget | fixed_price | 20 | 0 | 20 | 0 | yes | 333.3333333333333 | 364.0 | False |
| 203 | luxury | fixed_price | 20 | 0 | 20 | 0 | yes | Invalid Request | Invalid Request | True |
| 204 | budget | minute | 0 | 10 | 20 | 0 | yes | 520 | 364.0 | False |
| 205 | luxury | minute | 0 | 10 | 20 | 0 | yes | 800 | 728.0 | False |
| 206 | budget | fixed_price | 0 | 10 | 20 | 0 | yes | 333.3333333333333 | 364.0 | False |
| 207 | luxury | fixed_price | 0 | 10 | 20 | 0 | yes | Invalid Request | Invalid Request | True |
| 208 | budget | minute | 10 | 10 | 20 | 0 | yes | 520 | 364.0 | False |
| 209 | luxury | minute | 10 | 10 | 20 | 0 | yes | 800 | 728.0 | False |
| 210 | budget | fixed_price | 10 | 10 | 20 | 0 | yes | 333.3333333333333 | 364.0 | False |
| 211 | luxury | fixed_price | 10 | 10 | 20 | 0 | yes | Invalid Request | Invalid Request | True |
| 212 | budget | minute | 20 | 10 | 20 | 0 | yes | 520 | 364.0 | False |
| 213 | luxury | minute | 20 | 10 | 20 | 0 | yes | 800 | 728.0 | False |
| 214 | budget | fixed_price | 20 | 10 | 20 | 0 | yes | 333.3333333333333 | 364.0 | False |
| 215 | luxury | fixed_price | 20 | 10 | 20 | 0 | yes | Invalid Request | Invalid Request | True |
| 216 | budget | minute | 0 | 0 | 0 | 10 | yes | 0 | 0.0 | True |
| 217 | luxury | minute | 0 | 0 | 0 | 10 | yes | 0 | 0.0 | True |
| 218 | budget | fixed_price | 0 | 0 | 0 | 10 | yes | 0 | 0.0 | True |
| 219 | luxury | fixed_price | 0 | 0 | 0 | 10 | yes | Invalid Request | Invalid Request | True |
| 220 | budget | minute | 10 | 0 | 0 | 10 | yes | 0 | 0.0 | True |
| 221 | luxury | minute | 10 | 0 | 0 | 10 | yes | 0 | 0.0 | True |
| 222 | budget | fixed_price | 10 | 0 | 0 | 10 | yes | 0 | 0.0 | True |
| 223 | luxury | fixed_price | 10 | 0 | 0 | 10 | yes | Invalid Request | Invalid Request | True |
| 224 | budget | minute | 20 | 0 | 0 | 10 | yes | 0 | 0.0 | True |
| 225 | luxury | minute | 20 | 0 | 0 | 10 | yes | 0 | 0.0 | True |
| 226 | budget | fixed_price | 20 | 0 | 0 | 10 | yes | 0 | 0.0 | True |
| 227 | luxury | fixed_price | 20 | 0 | 0 | 10 | yes | Invalid Request | Invalid Request | True |
| 228 | budget | minute | 0 | 10 | 0 | 10 | yes | 0 | 0.0 | True |
| 229 | luxury | minute | 0 | 10 | 0 | 10 | yes | 0 | 0.0 | True |
| 230 | budget | fixed_price | 0 | 10 | 0 | 10 | yes | 0 | 0.0 | True |
| 231 | luxury | fixed_price | 0 | 10 | 0 | 10 | yes | Invalid Request | Invalid Request | True |
| 232 | budget | minute | 10 | 10 | 0 | 10 | yes | 0 | 0.0 | True |
| 233 | luxury | minute | 10 | 10 | 0 | 10 | yes | 0 | 0.0 | True |
| 234 | budget | fixed_price | 10 | 10 | 0 | 10 | yes | 0 | 0.0 | True |
| 235 | luxury | fixed_price | 10 | 10 | 0 | 10 | yes | Invalid Request | Invalid Request | True |
| 236 | budget | minute | 20 | 10 | 0 | 10 | yes | 0 | 0.0 | True |
| 237 | luxury | minute | 20 | 10 | 0 | 10 | yes | 0 | 0.0 | True |
| 238 | budget | fixed_price | 20 | 10 | 0 | 10 | yes | 0 | 0.0 | True |
| 239 | luxury | fixed_price | 20 | 10 | 0 | 10 | yes | Invalid Request | Invalid Request | True |
| 240 | budget | minute | 0 | 0 | 10 | 10 | yes | 260 | 182.0 | False |
| 241 | luxury | minute | 0 | 0 | 10 | 10 | yes | 400 | 364.0 | False |
| 242 | budget | fixed_price | 0 | 0 | 10 | 10 | yes | 166.66666666666666 | 0.0 | False |
| 243 | luxury | fixed_price | 0 | 0 | 10 | 10 | yes | Invalid Request | Invalid Request | True |
| 244 | budget | minute | 10 | 0 | 10 | 10 | yes | 260 | 182.0 | False |
| 245 | luxury | minute | 10 | 0 | 10 | 10 | yes | 400 | 364.0 | False |
| 246 | budget | fixed_price | 10 | 0 | 10 | 10 | yes | 166.66666666666666 | 182.0 | False |
| 247 | luxury | fixed_price | 10 | 0 | 10 | 10 | yes | Invalid Request | Invalid Request | True |
| 248 | budget | minute | 20 | 0 | 10 | 10 | yes | 260 | 182.0 | False |
| 249 | luxury | minute | 20 | 0 | 10 | 10 | yes | 400 | 364.0 | False |
| 250 | budget | fixed_price | 20 | 0 | 10 | 10 | yes | 166.66666666666666 | 182.0 | False |
| 251 | luxury | fixed_price | 20 | 0 | 10 | 10 | yes | Invalid Request | Invalid Request | True |
| 252 | budget | minute | 0 | 10 | 10 | 10 | yes | 260 | 182.0 | False |
| 253 | luxury | minute | 0 | 10 | 10 | 10 | yes | 400 | 364.0 | False |
| 254 | budget | fixed_price | 0 | 10 | 10 | 10 | yes | 166.66666666666666 | 182.0 | False |
| 255 | luxury | fixed_price | 0 | 10 | 10 | 10 | yes | Invalid Request | Invalid Request | True |
| 256 | budget | minute | 10 | 10 | 10 | 10 | yes | 260 | 182.0 | False |
| 257 | luxury | minute | 10 | 10 | 10 | 10 | yes | 400 | 364.0 | False |
| 258 | budget | fixed_price | 10 | 10 | 10 | 10 | yes | 125 | 100.10000000000001 | False |
| 259 | luxury | fixed_price | 10 | 10 | 10 | 10 | yes | Invalid Request | Invalid Request | True |
| 260 | budget | minute | 20 | 10 | 10 | 10 | yes | 260 | 182.0 | False |
| 261 | luxury | minute | 20 | 10 | 10 | 10 | yes | 400 | 364.0 | False |
| 262 | budget | fixed_price | 20 | 10 | 10 | 10 | yes | 166.66666666666666 | 182.0 | False |
| 263 | luxury | fixed_price | 20 | 10 | 10 | 10 | yes | Invalid Request | Invalid Request | True |
| 264 | budget | minute | 0 | 0 | 20 | 10 | yes | 520 | 364.0 | False |
| 265 | luxury | minute | 0 | 0 | 20 | 10 | yes | 800 | 728.0 | False |
| 266 | budget | fixed_price | 0 | 0 | 20 | 10 | yes | 333.3333333333333 | 364.0 | False |
| 267 | luxury | fixed_price | 0 | 0 | 20 | 10 | yes | Invalid Request | Invalid Request | True |
| 268 | budget | minute | 10 | 0 | 20 | 10 | yes | 520 | 364.0 | False |
| 269 | luxury | minute | 10 | 0 | 20 | 10 | yes | 800 | 728.0 | False |
| 270 | budget | fixed_price | 10 | 0 | 20 | 10 | yes | 333.3333333333333 | 364.0 | False |
| 271 | luxury | fixed_price | 10 | 0 | 20 | 10 | yes | Invalid Request | Invalid Request | True |
| 272 | budget | minute | 20 | 0 | 20 | 10 | yes | 520 | 364.0 | False |
| 273 | luxury | minute | 20 | 0 | 20 | 10 | yes | 800 | 728.0 | False |
| 274 | budget | fixed_price | 20 | 0 | 20 | 10 | yes | 333.3333333333333 | 364.0 | False |
| 275 | luxury | fixed_price | 20 | 0 | 20 | 10 | yes | Invalid Request | Invalid Request | True |
| 276 | budget | minute | 0 | 10 | 20 | 10 | yes | 520 | 364.0 | False |
| 277 | luxury | minute | 0 | 10 | 20 | 10 | yes | 800 | 728.0 | False |
| 278 | budget | fixed_price | 0 | 10 | 20 | 10 | yes | 333.3333333333333 | 364.0 | False |
| 279 | luxury | fixed_price | 0 | 10 | 20 | 10 | yes | Invalid Request | Invalid Request | True |
| 280 | budget | minute | 10 | 10 | 20 | 10 | yes | 520 | 364.0 | False |
| 281 | luxury | minute | 10 | 10 | 20 | 10 | yes | 800 | 728.0 | False |
| 282 | budget | fixed_price | 10 | 10 | 20 | 10 | yes | 333.3333333333333 | 364.0 | False |
| 283 | luxury | fixed_price | 10 | 10 | 20 | 10 | yes | Invalid Request | Invalid Request | True |
| 284 | budget | minute | 20 | 10 | 20 | 10 | yes | 520 | 364.0 | False |
| 285 | luxury | minute | 20 | 10 | 20 | 10 | yes | 800 | 728.0 | False |
| 286 | budget | fixed_price | 20 | 10 | 20 | 10 | yes | 333.3333333333333 | 364.0 | False |
| 287 | luxury | fixed_price | 20 | 10 | 20 | 10 | yes | Invalid Request | Invalid Request | True |

## Tests for invalid input

| $id | type | plan | distance | planned_distance | time | planned_time | inno_discount | $result | $expected | $ok |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 288 | luxury | minute | 0 | 0 | 10 | 0 | no | 400 | 400 | True |
| 289 | bebra | minute | 0 | 0 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 290 | luxury | bebra | 0 | 0 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 291 | luxury | minute | -10 | 0 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 292 | luxury | minute | 0 | -10 | 10 | 0 | no | Invalid Request | Invalid Request | True |
| 293 | luxury | minute | 0 | 0 | -10 | 0 | no | Invalid Request | Invalid Request | True |
| 294 | luxury | minute | 0 | 0 | 10 | -10 | no | 400 | Invalid Request | False |
| 295 | luxury | minute | 0 | 0 | 10 | 0 | bebra | Invalid Request | Invalid Request | True |

## Issues
- `fixed_price` is completely broken:
  - Sometimes does not work (test 18)
  - Sometimes wrong formula price = time * 16.(6) (tests 18, 26, 50)
  - Sometimes another wrown formula (test 114)
- Wrong `budget` `minute` rate (test 96)
- Inno discount does not work (test 169)
- No data bounds validation on `planned_time` (test 294)
  - Actually value is not used but `planned_distance` is also not used but checked

