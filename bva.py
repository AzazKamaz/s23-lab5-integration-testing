import asyncio
import csv
import sys
import urllib.request
import urllib.parse
import httpx
import json


key = 'AKfycbx8vetYY9K8yM3a46_mZJX2kMgbRgOEBb1_FlgYlMsOuPM5C5aw0FwTZDQ3StsjO1J0Wg'
email = 'a.krotov@innopolis.university'

url_base = f'https://script.google.com/macros/s/{key}/exec'

parameters = {
    'service': 'calculatePrice',
    'email': email,
}

parameters_tested = {
    'type': ['budget', 'luxury'],  # , 'bebra'],
    'plan': ['minute', 'fixed_price'],  # , 'bebra'],
    'distance': [0, 10, 20],  # , -10],
    'planned_distance': [0, 10],  # , -10],
    'time': [0, 10, 20],  # , -10],
    'planned_time': [0, 10],  # , -10],
    'inno_discount': ['no', 'yes'],  # , 'bebra'],
}

parametes_estimate = {
    'minute_rate_budget': 20,
    'minute_rate_luxury': 40,
    'fixed_rate': 11,
    'max_deviation': 0.07,
    'inno_discount': 0.09,
}


def generate(**kwargs):
    if len(kwargs) == 0:
        yield {}
        return

    key, values = kwargs.popitem()
    for value in values:
        for rest in generate(**kwargs):
            yield {key: value, **rest}


def estimate(query):
    query = {**query}
    if query['type'] not in ['budget', 'luxury']:
        return 'Invalid Request'
    if query['plan'] not in {'budget': ['minute', 'fixed_price'], 'luxury': ['minute']}[query['type']]:
        return 'Invalid Request'
    if query['inno_discount'] not in ['no', 'yes']:
        return 'Invalid Request'
    if any(query[i] < 0 for i in ['distance', 'planned_distance', 'time', 'planned_time']):
        return 'Invalid Request'
    if query['plan'] == 'fixed_price':
        l, r = 1 - parametes_estimate['max_deviation'], 1 + \
            parametes_estimate['max_deviation']
        if not (query['planned_distance'] * l <= query['distance'] <= query['planned_distance'] * r):
            query['plan'] = 'minute'
        if not (query['planned_time'] * l <= query['time'] <= query['planned_time'] * r):
            query['plan'] = 'minute'
        else:
            price = parametes_estimate['fixed_rate'] * query['distance']
    if query['plan'] == 'minute':
        price = parametes_estimate[f'minute_rate_{query["type"]}'] * \
            query['time']
    if query['inno_discount'] == 'yes':
        price *= 1 - parametes_estimate['inno_discount']
    return price


async def run_case(client, params):
    query = {**parameters, **params}
    result = (await client.get(f'{url_base}?{urllib.parse.urlencode(query)}')).text
    if result != 'Invalid Request':
        result = json.loads(result)['price']
    expected = estimate(query)
    return (result, expected, result == expected)


async def main():
    limits = httpx.Limits(max_connections=50, max_keepalive_connections=0)
    async with httpx.AsyncClient(follow_redirects=True, limits=limits) as client:
        cases = list(generate(**parameters_tested))
        print(len(cases), file=sys.stderr)
        results = await asyncio.gather(*(run_case(client, case) for case in cases))

    writer = csv.DictWriter(
        sys.stdout,
        ['$id', *parameters_tested.keys(), '$result', '$expected', '$ok'],
        strict=True,
    )
    writer.writeheader()

    for i, (case, (result, expected, ok)) in enumerate(zip(cases, results)):
        writer.writerow({**case, '$id': i, '$result': result,
                        '$expected': expected, '$ok': ok})


if __name__ == '__main__':
    asyncio.run(main())
